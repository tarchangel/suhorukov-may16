package ru.paderov.sample.gen;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Usr on 28.05.2016.
 */
public class IndexGenerator {
    private static final String fileHeader =
            "<!DOCTYPE html>\n" +
                    "<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>Directory {0}</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<table>\n" +
                    "    <tr>\n" +
                    "        <td>Name</td>\n" +
                    "        <td>Size</td>\n" +
                    "        <td>Modification date</td>\n" +
                    "    </tr>";

    private static final String filePATERN =
            "    <tr>\n" +
                    "        <td><a href=\"{0}\">{1}</a> </td>" +
                    "        <td>{2}</td>\n" +
                    "        <td>{3}</td>\n" +
                    "    </tr>";

    private static final String fileBOTTOM =
            "</table>\n" +
                    "</body>\n" +
                    "</html>";

    Comparator comp = new Comparator() {
        public int compare(Object o1, Object o2) {
            File f1 = (File) o1;
            File f2 = (File) o2;
            if (f1.isDirectory() && !f2.isDirectory()) {
                return -1;
            } else if (!f1.isDirectory() && f2.isDirectory()) {
                return 1;
            } else {
                return f1.compareTo(f2);
            }
        }
    };

    public void generateIndexHtml(File file, Writer writer) throws IOException {
        if (file.isDirectory()) {
            File files[] = file.listFiles();
            SimpleDateFormat sdf = new SimpleDateFormat("YYY-mm-dd HH:MM:ss");
            Arrays.sort(files, comp);
            for (File f : files) {
                long time = f.lastModified();
                String path = f.getName();
                String size;
                if (f.isDirectory()) {
                    path = f.getName() + "/";
                    size = "DIR";
                } else {
                    if (f.length() > 1024) {
                        size = f.length() / 1024 + "KB";
                    } else {
                        size = f.length() + "B";
                    }
                }
                writer.write(MessageFormat.format(filePATERN, f.getName(), path, size, sdf.format(new Date(time))));
            }
        } else {
            throw new IOException("Not a directory");
        }
    }

    public static void main(String[] args) throws Exception {
        IndexGenerator gen = new IndexGenerator();
        try (Writer writer = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(args[0] + "\\" + "index.html")), "UTF-8")) {
            File dir = new File(args[0]);
            writer.write(MessageFormat.format(fileHeader, dir));
            writer.write(MessageFormat.format(filePATERN, dir.getParent(), "..", "", "")) ;
            gen.generateIndexHtml(dir, writer);
            writer.write(fileBOTTOM);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
