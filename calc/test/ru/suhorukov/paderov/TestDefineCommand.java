package ru.suhorukov.paderov;

import junit.framework.TestCase;
import ru.suhorukov.paderov.calc.Command;
import ru.suhorukov.paderov.calc.CommandFactory;
import ru.suhorukov.paderov.calc.ExecuteContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Usr on 14.05.2016.
 */
public class TestDefineCommand extends TestCase {
    public void testCorrectDefine() {
        Command define = new CommandFactory().getCommandByName("DEFINE");
        assertNotNull(define);
        String[] args = {"DEFINE", "PI", "3.14"};
        Map<String, Double> defines = new HashMap<>();
        ExecuteContext ctx = new ExecuteContext(args, null, defines);
        define.execute(ctx);
        assertEquals(1, defines.size());
        assertTrue(defines.containsKey("PI"));
        assertEquals(3.14, defines.get("PI"), 1e-9);
    }
    public void testNullContextDefine(){
        Command define = new CommandFactory().getCommandByName("DEFINE");
        try {
            define.execute(null);
            fail("Expect NPE");
        } catch (NullPointerException e){
            assertTrue(true);
        }
    }
}
