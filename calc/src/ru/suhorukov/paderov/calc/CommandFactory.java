package ru.suhorukov.paderov.calc;

import ru.suhorukov.paderov.calc.command.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Usr on 12.05.2016.
 */
public class CommandFactory {
    private static Map<String, Command> mapCmd = new HashMap<>();
        public Command getCommandByName(String cmdname) {
            Properties p = new Properties();
            try{
                InputStream in = CommandFactory.class.getResourceAsStream("CommandFactory.properties");
                p.load(in);//load(Reader), loadFromXML(InputStream)
                for(String name: p.stringPropertyNames()){
                    String cmd = p.getProperty(name);
                    mapCmd.put(name,(Command)Class.forName(cmd).newInstance());
                }
                in.close();
            }catch(IOException e){
                e.printStackTrace();
            }catch (ClassNotFoundException e){
                e.printStackTrace();
            }
             catch (IllegalAccessException e){
                 e.printStackTrace();
             }
             catch (InstantiationException e) {
                e.printStackTrace();
            }
            return mapCmd.get(cmdname);
        }

}
