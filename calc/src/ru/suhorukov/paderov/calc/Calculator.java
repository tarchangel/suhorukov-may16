package ru.suhorukov.paderov.calc;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by Usr on 12.05.2016.
 */

public class Calculator {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.calculate();
    }


    private Scanner scanner;

    public void calculate() {
        CommandFactory factory = new CommandFactory();
        Stack<Double> stack = new Stack<>();
        Map<String, Double> defines = new HashMap<>();
        while (true) {
            try {
                System.out.println("Введите команду и аргумент");
                Scanner scanner = new Scanner(System.in);
                String line = scanner.nextLine();
                String args[] = line.split(" ");
                String commandName = args[0];
                Command cmd = factory.getCommandByName(commandName);

                ExecuteContext ctx = new ExecuteContext(args, stack, defines);
                cmd.execute(ctx);
                System.out.println(stack);
            } catch (Exception e) {
                System.out.println("Попробуйте ещё раз");
            }
            }
        }
    }
