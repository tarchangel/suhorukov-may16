package ru.suhorukov.paderov.calc.command;

import ru.suhorukov.paderov.calc.Command;
import ru.suhorukov.paderov.calc.ExecuteContext;
import ru.suhorukov.paderov.calc.anno.Precondition;

import java.util.Stack;

/**
 * Created by Usr on 14.05.2016.
 */
@Precondition(stackSize = 2)
public class AddCommand implements Command {
    @Override
    public void  execute(ExecuteContext ctx){
        Stack<Double> stack = ctx.getStack();
        Double v1 = stack.pop();
        Double v2 = stack.pop();
        double rs = v1 + v2;
        stack.push(rs);
    }
}
