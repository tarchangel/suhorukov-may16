package ru.suhorukov.paderov.calc.command;

import ru.suhorukov.paderov.calc.Command;
import ru.suhorukov.paderov.calc.ExecuteContext;

import java.util.Stack;

/**
 * Created by yypadero on 17.05.2016.
 */
public class PopCommand implements Command{
    @Override
    public void execute(ExecuteContext ctx) {
        Stack<Double> stack = ctx.getStack();
        System.out.println(stack.pop());
    }
}
