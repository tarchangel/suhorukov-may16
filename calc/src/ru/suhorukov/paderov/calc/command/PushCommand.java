package ru.suhorukov.paderov.calc.command;

import ru.suhorukov.paderov.calc.Command;
import ru.suhorukov.paderov.calc.ExecuteContext;

import java.util.Stack;

/**
 * Created by Kitahara on 16.05.2016.
 */
public class PushCommand implements Command {
    @Override
    public void execute(ExecuteContext ctx){
        Stack<Double> stack = ctx.getStack();
        String args[] = ctx.getArgs();
        Double arg = Double.valueOf(args[1]);
        stack.push(arg);

    }
}
