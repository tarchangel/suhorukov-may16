package ru.suhorukov.paderov.calc.command;

import ru.suhorukov.paderov.calc.Command;
import ru.suhorukov.paderov.calc.ExecuteContext;

/**
 * Created by yypadero on 23.05.2016.
 */
public class ExitCommand implements Command {
    @Override
    public void execute(ExecuteContext ctx){
        System.exit(0);
    }
}
