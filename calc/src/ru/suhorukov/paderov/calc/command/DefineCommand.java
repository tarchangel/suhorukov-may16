package ru.suhorukov.paderov.calc.command;

import ru.suhorukov.paderov.calc.Command;
import ru.suhorukov.paderov.calc.ExecuteContext;
import ru.suhorukov.paderov.calc.anno.In;
import ru.suhorukov.paderov.calc.anno.Precondition;
import ru.suhorukov.paderov.calc.anno.ResourceType;

import java.util.Collection;
import java.util.Map;

/**
 * Created by Usr on 14.05.2016.
 */
@Precondition(argSize = 2)
public class DefineCommand implements Command{
    @In(arg = ResourceType.DEFINES)
    private Map<String,double> defines;
    @Override
    public void execute(String[] args){
        String args[] = ctx.getArgs();
        ctx.getDefines().put(args[0], Double.valueOf(args[1]));
    }
}
