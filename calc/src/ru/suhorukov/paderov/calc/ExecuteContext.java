package ru.suhorukov.paderov.calc;

import java.util.Map;
import java.util.Stack;

/**
 * Created by Usr on 12.05.2016.
 */
public class ExecuteContext {
    private String[] args;
    private Stack<Double> stack;
    private Map<String, Double> defines;

    public ExecuteContext(String[] args, Stack<Double> stack, Map<String, Double> defines) {
        this.args = args;
        this.stack = stack;
        this.defines = defines;
    }

    public String[] getArgs() {
        return args;
    }

    public Stack<Double> getStack() {
        return stack;
    }

    public Map<String, Double> getDefines() {
        return defines;
    }
}