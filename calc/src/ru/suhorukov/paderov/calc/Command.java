package ru.suhorukov.paderov.calc;

import java.util.Stack;

/**
 * Created by Usr on 12.05.2016.
 */
public interface Command {
     void execute(ExecuteContext ctx);
    }

