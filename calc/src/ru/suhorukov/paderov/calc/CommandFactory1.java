package ru.suhorukov.paderov.calc;

import ru.suhorukov.paderov.calc.command.*;

/**
 * Created by Usr on 12.05.2016.
 */
public class CommandFactory1 {
    public Command getCommandByName(String name) {
        switch (name) {
            case "ADD":
                return new AddCommand();
            case "DEFINE":
                return new DefineCommand();
            case "PUSH":
                return new PushCommand();
            case "PRINT":
                return new PrintCommand();
            case "POP":
                return new PopCommand();
            default:
                return null;
        }
    }
}
