import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Main object = new Main();
        object.reader(new String[]{"TextReader\\src\\text.txt"});
    }

    public void reader(String[] filename) {
        try {
            Reader file = new InputStreamReader(new BufferedInputStream(new FileInputStream(filename[0])));
            int read;
            StringBuilder str = new StringBuilder();
            while ((read = file.read()) != -1) {
                if (Character.isLetterOrDigit(read)) {
                    str.append((char) read);
                } else {
                    System.out.println(str);
                    str.setLength(0);
                }
            }

        } catch (IOException e) {
            System.out.println("Вы не ввели название файла  " + e);
        }
    }/* List<Map.Entry<String, Integer>> list = new ArrayList<>(words.entrySet());
    Collections.sort(list, (a, b) ->{
        int rs = b.getValue().compareTo(a.getValue());
        if (rs == 0) {
            rs = a.getKey().compareTo(b.getKey());
        }
        return rs; **/
}
