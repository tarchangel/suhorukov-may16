import java.lang.reflect.Array;

/**
 * Created by Usr on 26.05.2016.
 */
public class BunchOfExceptions implements ExceptionGenerator {
    public static void main(String[] args) {
        BunchOfExceptions obj = new BunchOfExceptions();
        try {
            obj.generateNullPointerException();
            obj.generateClassCastException();
            obj.generateNumberFormatException();
            obj.generateStackOverflowError();
            obj.generateOutOfMemoryError();
            obj.generateMyException();
        } catch (NullPointerException e){
            System.out.println(e);
        } catch (ClassCastException e){
            System.out.println(e);
        } catch (NumberFormatException e){
            System.out.println(e);
        } catch (StackOverflowError e){
            System.out.println(e);
        } catch (OutOfMemoryError e) {
            System.out.println(e);
        } catch (MyException e) {
            System.out.println(e);
        }
    }

    @Override
    public void generateNullPointerException() {
            Integer num = null;
            num.toString();

    }

    @Override
    public void generateClassCastException() {
        String s = (String)new Object();
    }

    @Override
    public void generateNumberFormatException() {
        Integer.parseInt("asdasdasd");
    }

    @Override
    public void generateStackOverflowError() {
        generateStackOverflowError();
    }

    @Override
    public void generateOutOfMemoryError() {
        long[] arr = new long[1000_000_000];
    }

    @Override
    public void generateMyException() throws MyException {
        throw new MyException("Test");
    }
}