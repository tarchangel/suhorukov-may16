
package ru.suhorukov.paderov.httpserver;

import ru.paderov.sample.gen.IndexGenerator;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import static java.lang.System.in;

/**
 * Created by yypadero on 01.06.2016.
 */
public class HttpServer {
    public HttpServer(String[] args) {
        System.out.println();
    }

    public static void main(String[] args) {
        try {
            ServerSocket ss = new ServerSocket(8123);
            while (true) {
                Socket clientS = ss.accept();
                System.out.println("socket detected");
                new Thread(new Client(clientS)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class Client implements Runnable {
        private Socket clientS;
        private InputStream is;
        private OutputStream os;

        private Client(Socket clientS) throws IOException {
            this.clientS = clientS;
            this.is = clientS.getInputStream();
            this.os = clientS.getOutputStream();
        }

        @Override
        public void run() {
            try {
                String[] args = readInputHeaders();
                String cmd = args[0].trim().toUpperCase();
                String path = args[1];
                String homedir = "C:\\";

                if (cmd.equals("GET")) {
                    writeResponse(homedir + path);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    clientS.close();
                } catch (IOException e) {
                    e.printStackTrace();

                }
            }
            System.err.println("Client processing finished");
        }

        private void writeResponse(String s) throws Exception {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
            IndexGenerator ig = new IndexGenerator();
            ig.indexGenerator(s+"\\", bw);
        }

        private String[] readInputHeaders() throws IOException {
            StringBuilder sb = new StringBuilder();
            int c;
            while ((c = is.read()) != -1 && c != 10 && c != 13) {
                sb.append((char) c);
            }
            String data = sb.toString();
            String args[] = data.split(" ");
            return args;
        }
    }
}



